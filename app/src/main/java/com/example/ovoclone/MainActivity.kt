package com.example.ovoclone

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    val textValue: MutableLiveData<String> = MutableLiveData<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val editText = findViewById<EditText>(R.id.editTextTextPersonName3)
        val textViewKetentuan = findViewById<TextView>(R.id.textViewKetentuan)
        val textViewKebijakan = findViewById<TextView>(R.id.textViewKebijakan)

        textViewKetentuan.setOnClickListener {
            startActivity(Intent(this, SyaratActivity::class.java))
        }

        textViewKebijakan.setOnClickListener {
            startActivity(Intent(this, KebijakanActivity::class.java))
        }

        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                textValue.value = s.toString()
            }
        })

        textValue.observe(this, { value ->
            run {
                val buttonLanjutkan = findViewById<Button>(R.id.lanjutkan)
                if(value.length > 0) {
                    buttonLanjutkan.setBackgroundColor(Color.parseColor("#C8C8C8"))
                    buttonLanjutkan.isEnabled = true
                } else {
                    buttonLanjutkan.setBackgroundColor(Color.parseColor("#9C9C9C"))
                    buttonLanjutkan.isEnabled = false
                }

            }
        })
    }
}